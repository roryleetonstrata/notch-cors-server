/*
 * Primary API file
 *
 */

const server = require('./lib/server');

var app = {};

app.init = () => {
	server.init();
};

app.init();

module.exports = app;