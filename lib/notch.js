
const fs = require('fs');
const http = require('http');
const path = require('path');
const stringDecoder = require('string_decoder').StringDecoder;
const url = require('url')
const util = require('util');

const helpers = require('./helpers');

var lib = {};
var str = "";

lib.getExposedParams = (notchPort, callback) => {
	var controls = "";

	var parsedURL = url.parse('http://localhost:'+notchPort+'/control', true);
	var hostname = parsedURL.hostname;
	var path = parsedURL.path;

	var decoder = new stringDecoder('utf-8');

	var requestDetails = {
		'protocol': 'http:',
		'hostname': hostname,
		'port': parsedURL.port,
		'method': 'GET',
		'path': path
	}

	var req = http.request(requestDetails, (res) => {
		var status = res.statusCode;

		if (status == 200 || status == 201) {
			res.on('data', function (chunk) {
			    controls += decoder.write(chunk);
			 });
			 res.on('end', function () {
			    callback(false, helpers.parseJsonToObject(controls));
			 });
		} else {
			callback(status, {'Error': 'Could not get composition parameters.'});
		}
	});

	req.on('error', (e) => {
		callback(e);
	});

	req.end();
};

lib.updateParams = (notchPort, data, callback) => {
	var decoder = new stringDecoder('utf-8');
	var controlsURL = 'http://localhost:'+notchPort+'/control?'
	Object.keys(data.payload).forEach((id, index) => {
		setTimeout( () => {
			http.get(controlsURL+'uid='+id+'&value='+data.payload[id], (res) => {
				var statusCode = res.statusCode;
				var status = "";
				if (statusCode == 200 || statusCode == 201) {
					res.on('data', function (chunk) {
					    status += decoder.write(chunk);
					 });
					 res.on('end', function () {
					    callback(false, helpers.parseJsonToObject(status));
					 });
				} else {
					callback(statusCode, {'Error': 'Could not update control parameters.'});
				}
			});
		}, 10*index);
	});
};

module.exports = lib;