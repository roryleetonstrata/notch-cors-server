
var notch = require('./notch');
var helpers = require('./helpers');
var config = require('./config')
const util = require('util');
var handlers = {};



// Composition
handlers.composition = (data, callback) => {
	var acceptableMethods = ['get', 'post'];
	// console.log("Data: "+util.inspect(data, false, null, true));
	if ( acceptableMethods.indexOf(data.method) > -1 ) {
		handlers._composition[data.method](data, callback);
	} else {
		callback(405, {'Error' : 'Method not allowed'});
	}
};

handlers._composition = {};

handlers._composition.get = (data, callback) => {
	notch.getExposedParams(config.notchPort, (err, data) => {
		if (!err) {
			callback(200, data);
		} else {
			callback(err, "Could not get parameters from composition.");
		}
	});
};

handlers._composition.post = (data, callback) => {
	// console.log("Data: "+util.inspect(data, false, null, true));
	notch.updateParams(config.notchPort, data, (err, data) => {
		if (!err) {
			callback(200, data)
		} else {
			callback(err, "Could not update composition parameters.");
		}
	});
};

// Not Found
handlers.notFound = (data, callback) => {
  callback(404, {'Error' : 'Handler not found.'});
};

module.exports = handlers;