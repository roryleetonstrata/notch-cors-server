
var environments = {};

environments.local = {
	'httpPort': 91,
	'httpsPort': 92,
	'notchPort': 8910,
	'envName': 'local',
};

environments.staging = {
	'httpPort': 3000,
	'httpsPort': 3001,
	'notchPort': 8910,
	'envName': 'staging'
};


environments.production = {
	'httpPort': 80,
	'httpsPort': 443,
	'notchPort': 8910,
	'envName': 'production'
};

var currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';

var environmentToExport = typeof(environments[currentEnvironment]) == 'object' ? typeof(environments[currentEnvironment]) : environments.local;

module.exports = environmentToExport;