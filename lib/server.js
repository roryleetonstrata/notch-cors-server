
const fs = require('fs');
const http = require('http');
const https = require('https');
const path = require('path');
const url = require('url');
const util = require('util');

const config = require('./config');
const handlers = require('./handlers');
const helpers = require('./helpers');
const notch = require('./notch.js');
const stringDecoder = require('string_decoder').StringDecoder;

var server = {};

server.httpServer = http.createServer( (req, res) => {
	server.unifiedServer(req, res);
});

server.httpsServerOptions = {
	'key': fs.readFileSync(path.join(__dirname, '/../https/key.pem')),
	'cert': fs.readFileSync(path.join(__dirname,'/../https/cert.pem'))
};
server.httpsServer = https.createServer(server.httpsServerOptions, (req, res) => {
	server.unifiedServer(req, res);
});

server.unifiedServer = (req, res) => {
	const responseHeaders = {
		"Access-Control-Allow-Origin": "*",
		"Access-Control-Allow-Methods": "POST, GET, OPTIONS, DELETE, PUT",
		"Access-Control-Allow-Headers": "append,delete,entries,foreach,get,has,keys,set,values,Authorization,Content-Type",
		"Access-Control-Max-Age": 2592000,
		"Content-Type": "application/json"
	}

	var parsedURL = url.parse(req.url, true);

	var path = parsedURL.pathname;
	var trimmedPath = path.replace(/^\/+|\/+$/g,'');

	var method = req.method.toLowerCase();

	var queryStringObject = parsedURL.query;

	var headers = req.headers;
	var decoder = new stringDecoder('utf-8');
	var buffer = "";

	req.on('data', (data) => {
		buffer += decoder.write(data);
	});
	req.on('end', () => {
		buffer += decoder.end();

		if (method === "options") {
			res.writeHead(204, responseHeaders);
			res.end();
		} else {
			var chosenHandler = typeof(server.router[trimmedPath]) !== 'undefined' ? server.router[trimmedPath] : handlers.notFound;
			var data = {
				'trimmedPath': trimmedPath,
				'queryStringObject': queryStringObject,
				'method': method,
				'headers': headers,
				'payload': helpers.parseJsonToObject(buffer)
			};

			chosenHandler(data, (statusCode, payload) => {
				statusCode = typeof(statusCode) == 'number' ? statusCode : 200;
				payload = typeof(payload) == 'object' ? payload : {};
				payloadString = JSON.stringify(payload);
				
				res.writeHead(statusCode, responseHeaders);
				res.end(payloadString);

				console.log("Response: ", statusCode, payload);
			});
		};
	});
};

server.router = {
  'composition' : handlers.composition
};

server.init = () => {
	server.httpServer.listen(config.httpPort, () => {
		console.log("Server is listening on port "+config.httpPort+".");
	});
	server.httpsServer.listen(config.httpsPort, () => {
		console.log("Server is listening on port "+config.httpsPort+".");
	});
}

module.exports = server;