

var config = require('./config');

var helpers = {};

// Parse JSON
helpers.parseJsonToObject = (str) => {
  try {
    var obj = JSON.parse(str);
    return obj
  } catch(e) {
    return {};
  }
};

module.exports = helpers;