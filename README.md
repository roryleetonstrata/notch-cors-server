# Notch CORS Server #

Basic intermediary server to process requests between custom browser UIs and Notch compositions.

### Features ###

* Requests JSON containing all exposed parameters in composition and sends them to client application/browser.
* Takes JSON array of unique IDs and parameter values from client application/browser and passes them to Notch compostion.
* Version: 1.0.0.

### Setup ###

* Install NodeJS (currently using v12.18.0) and npm (currently using v6.14.5).
* Open terminal/command line, cd to repository, run `node index.js` or `npm run serve`.
* Port numbers are set in lib/config.js. Default config set to local.
* For default config, send GET/POST fetch requests to `http://localhost:91/composition`.

### Contact | Social Media ###

* hello@strata.systems
* [Instagram](https://www.instagram.com/strata.systems/)